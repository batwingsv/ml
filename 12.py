numb1 = int(input('Enter 1st value: '))
numb2 = int(input('Enter 2nd value: '))
operation_calc = input('Enter your operation sign:')

if operation_calc == '*':
    print(numb1, operation_calc, numb2, ' = ', numb1*numb2)
elif operation_calc == '+':
    print(numb1, operation_calc, numb2,  ' = ', numb1+numb2)
elif operation_calc == '-':
    print(numb1, operation_calc, numb2,  ' = ', numb1-numb2)
elif operation_calc == '/':
    print(numb1, operation_calc, numb2,  ' = ', numb1/numb2)
else:
    print('You entered wrong operation, please run your program again')
