import math

a = int(input('(a != 0) a = '))
b = int(input('b = '))
c = int(input('c = '))

discr = b*b - 4*a*c


if discr == 0:
    x1 = -b/2*a
    x2 = x1
elif discr > 0:
    x1 = (-1 * b + math.sqrt(discr))/(2*a)
    x2 = (-1 * b - math.sqrt(discr))/(2*a)
elif discr < 0:
    x1 = 'No results'
    x2 = x1

print('x1 = ', x1)
print('x2 = ', x2)
print('discr = ', discr)
