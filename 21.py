starting_temp_c = int(input('Starting temp in C: '))
starting_temp_f = int(input('Starting temp in F: '))
step = int(input('Step: '))

for t in range(starting_temp_c, starting_temp_f, step):
    f = (t*9/5)+32
    print(str(t) + 'c -> '+ str(f) + ' f')