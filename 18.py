import numpy as np

all_numbs = int(input('Enter quantity of numbers: '))
a = []
for i in range(all_numbs):
    a.append(int(input('Enter value: ')))
sign = str(input('Enter the sign: '))
if sign == '+':
    print(sum(a))
elif sign == '*':
    print(np.prod(a))
